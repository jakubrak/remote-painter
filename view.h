#ifndef VIEW_H
#define VIEW_H

#include <QMainWindow>
#include <QTimer>
#include <QHostAddress>
#include <QSettings>
#include <QApplication>
#include "outputboard.h"
#include "settingsdialog.h"

typedef QQueue<QPoint> PointQueue;
typedef QMultiMap<int, QString > ConnectionList;

namespace Ui {
    class View;
}

class View : public QMainWindow
{
    Q_OBJECT

public:
    explicit View(QString clientId, QWidget *parent = 0);
    void loadSettings();
    ~View();

public slots:
    void updateConnectionList(ConnectionList connectionList, QString tokenClientId, bool hasLock);

    void updateBoard(const QByteArray &pixels);
    void requestBoard(int connectionId, QString clientId);

    void lockChanged(bool lock, bool remote);

    void connected();
    void disconnected();

private slots:
    void connectAndListenSlot();
    void listenSlot();
    void disconnectAndStopListeningSlot();
    void saveSettings();
    void showContextMenu(const QPoint& pos);
    void tokenTimerTick();

signals:
    void connectAndListen(QString remoteAddr, short remotePort,
                          QString localAddr, short localPort);
    void listen(QString serverAddr, short serverPort);
    void disconnectAndStopListening();

    void responseBoard(const QByteArray &pixels, int id, QString clientId);

    void lineDrawn(PointQueue pointList, QColor color);
    void lineReceived(PointQueue pointList, QColor color);

    void clearBoard();
    void clearBoardReceived();

    void tryLock();
    void unlock();

private:
    Ui::View *ui;
    OutputBoard *board;
    QSettings settings;
    QMenu *contextMenu;
    QAction *clearBoardAction;
    QAction *tryLockAction;
    QAction *unlockAction;
    QAction *connectAndListenAction;
    QAction *listenAction;
    QAction *disconnectAction;
    QAction *settingsAction;
    QString clientId;
    SettingsDialog *settingsDialog;
    const QString settingsSavePath = QApplication::applicationDirPath() + "/settings.ini";
    QTimer *tokenTimer;
    int tokenTimerTickCounter;
    const int tokenTimerInterval = 1000;
    const int tokenTimerMaxTick = 10;
};

#endif // VIEW_H
