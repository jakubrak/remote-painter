#include "outputboard.h"
#include <QPainter>
#include <QDebug>
#include <QBuffer>
#include <QMenu>

#define POINT_QUEUE_MAX_SIZE 30 // points
#define TIMER_INTERVAL 100 // miliseconds

OutputBoard::OutputBoard(QObject *parent)
{
    connect(&timer, SIGNAL(timeout()), this, SLOT(timerTick()));
    setContextMenuPolicy(Qt::CustomContextMenu);
    image = QImage(size(), QImage::Format_Mono);
    QSize newSize = image.size().expandedTo(size());
    resizeImage(&image, newSize);
    clearBoardRemote();
    update();
    timer.start(TIMER_INTERVAL);
    lineColor = Qt::black;
}

void OutputBoard::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && !lock)
    {
        pointQueue.enqueue(event->pos());
        if(pointQueue.size() >= POINT_QUEUE_MAX_SIZE)
        {
            pointQueue.clear();
            pointQueue.enqueue(event->pos());
        }
        lastPoint = event->pos();
    }
}

void OutputBoard::mouseMoveEvent(QMouseEvent *event)
{
    if ((event->buttons() & Qt::LeftButton) && !lock)
    {
        drawLineSegment(lastPoint, event->pos(), lineColor);
        pointQueue.enqueue(event->pos());
        if(pointQueue.size() >= POINT_QUEUE_MAX_SIZE)
        {
            qDebug() << lineColor;
            emit lineDrawn(pointQueue, lineColor);
            pointQueue.clear();
            pointQueue.enqueue(event->pos());
        }
        lastPoint = event->pos();
    }
}

void OutputBoard::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && !lock)
    {
        drawLineSegment(lastPoint, event->pos(), lineColor);
        qDebug() << lineColor;
        pointQueue.enqueue(event->pos());
        emit lineDrawn(pointQueue, lineColor);
        pointQueue.clear();
        lastPoint = event->pos();
        //pointQueue.enqueue(event->pos());
    }
}

void OutputBoard::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, image, dirtyRect);
}

void OutputBoard::resizeEvent(QResizeEvent *event)
{
    if (width() > image.width() || height() > image.height()) {
        int newWidth = qMax(width() + 128, image.width());
        int newHeight = qMax(height() + 128, image.height());
        resizeImage(&image, QSize(newWidth, newHeight));
        update();
    }
    QWidget::resizeEvent(event);
}

void OutputBoard::drawLineSegment(const QPoint &startPoint, const QPoint &endPoint, QColor color)
{
    QPainter painter(&image);
    painter.setPen(QPen(color, 1, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawLine(startPoint, endPoint);
    int rad = (1 / 2) + 2;
    update(QRect(startPoint, endPoint).normalized()
                                     .adjusted(-rad, -rad, +rad, +rad));
}

void OutputBoard::resizeImage(QImage *image, const QSize &newSize)
{
    if (image->size() == newSize)
        return;
    QImage newImage(newSize, QImage::QImage::Format_Mono);
    newImage.fill(qRgb(255, 255, 255));
    QPainter painter(&newImage);
    *image = newImage;
    painter.drawImage(QPoint(0, 0), newImage);
}

QColor OutputBoard::getLineColor() const
{
    return lineColor;
}

void OutputBoard::setLineColor(const QColor &value)
{
    lineColor = value;
}

void OutputBoard::updateBoard(const QByteArray &pixels)
{
    image.loadFromData(pixels);
    update();
}

void OutputBoard::getBoard(QByteArray &pixels)
{
    QBuffer buffer(&pixels);
    image.save(&buffer, "PNG");
}

void OutputBoard::lineReceived(PointQueue pointQueue, QColor color)
{
    QPoint startPoint = pointQueue.dequeue();
    while(!pointQueue.isEmpty())
    {
        QPoint endPoint = pointQueue.dequeue();
        drawLineSegment(startPoint, endPoint, color);
        startPoint = endPoint;
    }
}

void OutputBoard::timerTick()
{
    if(!pointQueue.isEmpty())
    {
        emit lineDrawn(pointQueue, lineColor);
        pointQueue.clear();
        pointQueue.enqueue(lastPoint);
    }
}

void OutputBoard::clearBoardRemote()
{
    image.fill(qRgb(255, 255, 255));
    update();
}

void OutputBoard::clearBoardLocal()
{
    if(!lock)
    {
        image.fill(qRgb(255, 255, 255));
        update();
    }
}

void OutputBoard::setLock(bool lock)
{
    this->lock = lock;
}
