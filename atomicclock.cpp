#include "atomicclock.h"
#include <QHostInfo>
#include <QDebug>
#include <QDateTime>
#include <QByteArray>

AtomicClock::AtomicClock(QObject *parent) :
    udpSocket(new QUdpSocket(parent)),
    timer(new QTimer(parent)),
    elapsedTimer(new QElapsedTimer)
{
    connect(udpSocket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
    connect(udpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));
}
AtomicClock::~AtomicClock()
{
    udpSocket->close();
}

void AtomicClock::start()
{
    udpSocket->bind(QHostAddress::Any, 123, QAbstractSocket::DefaultForPlatform);
    //timer->start(1000);
    //elapsedTimer->start();
}

void AtomicClock::stop()
{

}

void AtomicClock::synchronize()
{
    QHostInfo info = QHostInfo::fromName("0.ubuntu.pool.ntp.org");
    QByteArray timeRequest(48, 0);
    timeRequest[0] = 0x1B;
    udpSocket->flush();
    QHostAddress hostAddress = info.addresses().first();
    quint16 port = 123;
    qint64 wc = udpSocket->writeDatagram(timeRequest, hostAddress, port);
    qDebug() << "Write " << wc << " bytes to " << hostAddress << " (port " << port << ")";
}
void AtomicClock::readPendingDatagrams()
{
    qDebug() << "Reading...";
    QByteArray newTime;
    QDateTime Epoch(QDate(1900, 1, 1));
    QDateTime unixStart(QDate(1970, 1, 1));
    while(udpSocket->hasPendingDatagrams())
    {
        newTime.resize(udpSocket->pendingDatagramSize());
        QHostAddress hostAddress;
        quint16 port;
        qint64 rc = udpSocket->readDatagram(newTime.data(), newTime.size(), &hostAddress, &port);
        qDebug() << "Received " << rc << " bytes from " << hostAddress << " (port " << port << ")";
        if(rc < 0)
        {
            qDebug() << "receiving error";
            break;
        }
    }

    qDebug() << newTime;

    QByteArray TransmitTimeStamp;
    TransmitTimeStamp = newTime.right(8);

    quint64 seconds = 0;
    for (int i=0; i<= 3; ++i)
    {
         seconds = (seconds << 8) | TransmitTimeStamp[i];
    }

    //ui->textEdit->append(QString::number(seconds, 10));
    QDateTime newestTime;
    newestTime.setTime_t(seconds - Epoch.secsTo(unixStart));
    qDebug() << newestTime.toString();
}

void AtomicClock::error(QAbstractSocket::SocketError socketError)
{
    qDebug() << socketError;
}


