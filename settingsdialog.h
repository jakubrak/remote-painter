#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QSettings>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();
    QString getRemoteIpAddress();
    void setRemoteIpAddress(const QString &ipAddress);
    int getRemotePort();
    void setRemotePort(int port);
    QString getLocalNetInterface();
    void setLocalNetInterface(const QString &netInterface);
    QString getLocalIpAddress();
    void setLocalIpAddress(const QString &ipAddress);
    int getLocalPort();
    void setLocalPort(int port);
    int getLineColor();
    void setLineColor(int color);

protected:
    void showEvent(QShowEvent * event);

private:
    Ui::SettingsDialog *ui;
    void fillComboBox();
};

#endif // SETTINGSDIALOG_H
