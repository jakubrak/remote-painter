#ifndef OUTPUTBOARD_H
#define OUTPUTBOARD_H

#include <QWidget>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QResizeEvent>
#include <QPoint>
#include <QImage>
#include <QSize>
#include <QQueue>
#include <QTimer>

typedef QQueue<QPoint> PointQueue;

class OutputBoard : public QWidget
{
    Q_OBJECT
public:
    OutputBoard(QObject *parent = 0);
    void updateBoard(const QByteArray &pixels);
    void getBoard(QByteArray &pixels);
    void setLineColor(const QColor &value);
    QColor getLineColor() const;

public slots:
    void lineReceived(PointQueue pointQueue, QColor color);
    void clearBoardRemote();
    void clearBoardLocal();
    void setLock(bool lock);
signals:
    void lineDrawn(PointQueue pointQueue, QColor color);
    void clearBoard();
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
private slots:
    void timerTick();
private:
    void drawLineSegment(const QPoint &startPoint, const QPoint &endPoint, QColor color);
    void resizeImage(QImage *image, const QSize &newSize);

    QImage image;
    PointQueue pointQueue;
    QPoint lastPoint;
    QTimer timer;
    bool lock;
    QColor lineColor;
};

#endif // OUTPUTBOARD_H
