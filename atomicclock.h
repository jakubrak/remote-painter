#ifndef ATOMICCLOCK_H
#define ATOMICCLOCK_H

#include <QObject>
#include <QUdpSocket>
#include <QTimer>
#include <QElapsedTimer>

class AtomicClock : public QObject {
    Q_OBJECT
public:
    AtomicClock(QObject *parent = 0);
    ~AtomicClock();
    void start();
    void stop();
    void synchronize();
private:

    QUdpSocket *udpSocket;
    QTimer *timer;
    QElapsedTimer *elapsedTimer;

private slots:

    void readPendingDatagrams();
    void error(QAbstractSocket::SocketError socketError);
};

#endif // ATOMICCLOCK_H
