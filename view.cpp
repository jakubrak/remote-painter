#include <QDebug>
#include <QNetworkInterface>
#include "view.h"
#include "ui_mainwindow.h"
#include "atomicclock.h"

View::View(QString clientId, QWidget *parent) :
    clientId(clientId),
    QMainWindow(parent),
    ui(new Ui::View)
{
    ui->setupUi(this);
    setWindowFlags(Qt::WindowStaysOnTopHint);

    setWindowTitle("ID: " + clientId);

    tokenTimer = new QTimer;
    connect(tokenTimer, SIGNAL(timeout()), this, SLOT(tokenTimerTick()));

    board = new OutputBoard(this);
    ui->verticalLayout->insertWidget(0, board);

    board->setFixedSize(640, 480);
    qRegisterMetaType<PointQueue>("PointQueue");
    connect(board, SIGNAL(lineDrawn(PointQueue, QColor)), this, SIGNAL(lineDrawn(PointQueue, QColor)));
    connect(this, SIGNAL(lineReceived(PointQueue, QColor)), board, SLOT(lineReceived(PointQueue, QColor)));
    connect(board, SIGNAL(clearBoard()), this, SIGNAL(clearBoard()));
    connect(this, SIGNAL(clearBoardReceived()), board, SLOT(clearBoardRemote()));
    connect(board, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));
    /*AtomicClock *atomicClock = new AtomicClock(this);
    atomicClock->start();
    atomicClock->synchronize();*/

    contextMenu = new QMenu;

    clearBoardAction = new QAction("Clear", this);
    connect(clearBoardAction, SIGNAL(triggered(bool)), board, SLOT(clearBoardLocal()));
    connect(clearBoardAction, SIGNAL(triggered(bool)), this, SIGNAL(clearBoard()));
    contextMenu->addAction(clearBoardAction);

    tryLockAction = new QAction("Try Lock", this);
    connect(tryLockAction, SIGNAL(triggered(bool)), this, SIGNAL(tryLock()));
    contextMenu->addAction(tryLockAction);
    tryLockAction->setDisabled(true);

    unlockAction = new QAction("Unlock", this);
    connect(unlockAction, SIGNAL(triggered(bool)), this, SIGNAL(unlock()));
    contextMenu->addAction(unlockAction);
    unlockAction->setDisabled(true);

    connectAndListenAction = new QAction("Connect && Listen", this);
    connect(connectAndListenAction, SIGNAL(triggered(bool)), this, SLOT(connectAndListenSlot()));
    contextMenu->addAction(connectAndListenAction);

    listenAction = new QAction("Listen", this);
    connect(listenAction, SIGNAL(triggered(bool)), this, SLOT(listenSlot()));
    contextMenu->addAction(listenAction);

    disconnectAction = new QAction("Disconnect && Stop Listening", this);
    connect(disconnectAction, SIGNAL(triggered(bool)), this, SLOT(disconnectAndStopListeningSlot()));
    contextMenu->addAction(disconnectAction);

    settingsAction = new QAction("Settings...", this);
    connect(settingsAction, SIGNAL(triggered(bool)), this, SLOT(saveSettings()));
    contextMenu->addAction(settingsAction);

    disconnectAction->setDisabled(true);

    settingsDialog = new SettingsDialog(this);

    loadSettings();

    ConnectionList connectionList;
    updateConnectionList(connectionList, "", false);
}

View::~View()
{
    delete ui;
}

void View::updateBoard(const QByteArray &pixels)
{
    //qDebug() << __FUNCTION__ << " data size: " << pixels->size();
    board->updateBoard(pixels);
}
void View::requestBoard(int connectionId, QString clientId)
{
    QByteArray pixels;
    board->getBoard(pixels);
    emit responseBoard(pixels, connectionId, clientId);
    //qDebug() << "requestBoard";
}

void View::updateConnectionList(ConnectionList connectionList, QString tokenClientId, bool hasLock)
{
    connectionList.insertMulti(-1, clientId);
    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(connectionList.values().size());
    ui->tableWidget->setColumnCount(4);
    int row = 0;
    for(ConnectionList::Iterator i = connectionList.begin(); i != connectionList.end(); ++i)
    {
        if(tokenClientId == i.value())
        {
            ui->tableWidget->setItem(row, 0, new QTableWidgetItem("X"));
            if(hasLock)
            {
                ui->tableWidget->setItem(row, 1, new QTableWidgetItem("X"));
            }
        }
        ui->tableWidget->setItem(row, 2, new QTableWidgetItem(QString::number(i.key())));
        ui->tableWidget->setItem(row, 3, new QTableWidgetItem(i.value()));
        ++row;
    }

    QStringList labels;
    labels.append("T");
    labels.append("L");
    labels.append("C");
    labels.append("ID");
    ui->tableWidget->setHorizontalHeaderLabels(labels);
}

void View::saveSettings()
{
    loadSettings();
    if(settingsDialog->exec() == QDialog::Accepted)
    {
        QSettings settings(settingsSavePath, QSettings::NativeFormat);
        settings.setValue("remoteaddr", settingsDialog->getRemoteIpAddress());
        settings.setValue("remoteport", settingsDialog->getRemotePort());
        settings.setValue("localport", settingsDialog->getLocalPort());
        settings.setValue("localinterface", settingsDialog->getLocalNetInterface());
        settings.setValue("linecolor", settingsDialog->getLineColor());
    }
    int color = settingsDialog->getLineColor();
    qDebug() << "color: " << color;
    if(color == 0)
        board->setLineColor(Qt::black);
    else
        board->setLineColor(Qt::white);
}

void View::loadSettings()
{
    QSettings settings(settingsSavePath, QSettings::NativeFormat);
    settingsDialog->setRemoteIpAddress(settings.value("remoteaddr", "127.0.0.1").toString());
    settingsDialog->setRemotePort(settings.value("remoteport", "5555").toInt());
    settingsDialog->setLocalPort(settings.value("localport", "5555").toInt());
    settingsDialog->setLocalNetInterface(settings.value("localinterface", "eth0").toString());
    settingsDialog->setLineColor(settings.value("linecolor", "0").toInt());
    int color = settingsDialog->getLineColor();
    qDebug() << "color: " << color;
    if(color == 0)
        board->setLineColor(Qt::black);
    else
        board->setLineColor(Qt::white);
}

void View::showContextMenu(const QPoint& pos)
{
    QPoint globalPos = board->mapToGlobal(pos);
    contextMenu->exec(globalPos);
}

void View::lockChanged(bool lock, bool remote)
{
    if(remote)
    {
        board->setLock(lock);
        tryLockAction->setDisabled(false);
        unlockAction->setDisabled(true);
        clearBoardAction->setDisabled(lock);
    }
    else
    {
        if(lock)
        {
            board->setLock(false);
            tokenTimerTickCounter = tokenTimerMaxTick;
            tokenTimer->start(tokenTimerInterval);
        }
        else
        {
            tokenTimer->stop();
        }
        tryLockAction->setDisabled(lock);
        unlockAction->setEnabled(lock);
        clearBoardAction->setDisabled(false);
    }
}

void View::connectAndListenSlot()
{
    qDebug() << "connect & Listening";
    connectAndListenAction->setDisabled(true);
    listenAction->setDisabled(true);
    emit connectAndListen(settingsDialog->getRemoteIpAddress(),
                          settingsDialog->getRemotePort(),
                          settingsDialog->getLocalIpAddress(),
                          settingsDialog->getLocalPort());
}

void View::listenSlot()
{
    emit listen(settingsDialog->getLocalIpAddress(),
                      settingsDialog->getLocalPort());
    connectAndListenAction->setDisabled(true);
    listenAction->setDisabled(true);
    disconnectAction->setDisabled(false);
    unlockAction->setDisabled(false);
    tryLockAction->setDisabled(false);
}

void View::disconnectAndStopListeningSlot()
{
    emit disconnectAndStopListening();
    disconnectAction->setDisabled(true);
    unlockAction->setDisabled(true);
    tryLockAction->setDisabled(true);
}

void View::connected()
{
    disconnectAction->setDisabled(false);
    unlockAction->setDisabled(false);
    tryLockAction->setDisabled(false);
    qDebug() << "Connected";
}

void View::disconnected()
{
    connectAndListenAction->setDisabled(false);
    listenAction->setDisabled(false);
    qDebug() << "Diconnected";
}

void View::tokenTimerTick()
{
    --tokenTimerTickCounter;
    ui->statusBar->showMessage("Lock time left: " + QString::number(tokenTimerTickCounter) + " second(s)");
    if(tokenTimerTickCounter == 0)
    {
        tokenTimer->stop();
        emit unlock();
    }
}


