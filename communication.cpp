#include "communication.h"

Communication::Communication(QString clientId) :
    clientId(clientId),
    tcpServer(new QTcpServer(this))
{
    connectionIdGen = 0;
    logicalTime = 0;

    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(openNewConnection()));
}

Communication::~Communication()
{
    QHash<int, QThread*>::iterator i = threads.begin();
    while (i != threads.end())
    {
        i.value()->quit();
        i.value()->wait();
        ++i;
    }
}

void Communication::openNewConnection()
{
    qRegisterMetaType< QQueue<QPoint> >();
    Connection *connection = new Connection(++connectionIdGen, tcpServer->nextPendingConnection());

    // move connection to new thread
    QThread *thread = new QThread(this);
    threads.insert(connectionIdGen, thread);
    connection->moveToThread(thread);
    connect(thread, SIGNAL(finished()), connection, SLOT(deleteLater()));
    connect(this, SIGNAL(send(QJsonObject, IdList)), connection, SLOT(send(QJsonObject, IdList)));
    connect(this, SIGNAL(sendBlocking(QJsonObject, IdList)), connection, SLOT(send(QJsonObject, IdList)), Qt::BlockingQueuedConnection);
    connect(connection, SIGNAL(received(QJsonObject, int)), this, SLOT(received(QJsonObject, int)));
    connect(connection, SIGNAL(error(int)), this, SLOT(connectionError(int)));
    thread->start();
}

void Communication::connectAndListen(QString remoteAddr, short remotePort, QString localAddr, short localPort)
{
    // try to connect to remote host
    QTcpSocket *tcpSocket = new QTcpSocket;
    tcpSocket->connectToHost(remoteAddr, remotePort);
    if(tcpSocket->waitForConnected(3000))
    {
        Connection *connection = new Connection(0, tcpSocket);
        QThread *thread = new QThread(this);
        threads.insert(0, thread);
        connection->moveToThread(thread);
        connect(thread, SIGNAL(finished()), connection, SLOT(deleteLater()));
        connect(this, SIGNAL(send(QJsonObject, IdList)), connection, SLOT(send(QJsonObject, IdList)));
        connect(this, SIGNAL(sendBlocking(QJsonObject, IdList)), connection, SLOT(send(QJsonObject, IdList)), Qt::BlockingQueuedConnection);
        connect(connection, SIGNAL(received(QJsonObject, int)), this, SLOT(received(QJsonObject, int)));
        connect(connection, SIGNAL(error(int)), this, SLOT(connectionError(int)));
        thread->start();
        tcpServer->listen(QHostAddress(localAddr), localPort);
        qDebug() << "Listening";

        QJsonObject json;
        json.insert("type", "joined");
        json.insert("clientId", clientId);
        IdList idList;
        idList.append(0);
        emit send(json, idList);
        emit connected();
    }
    else
    {
        emit disconnected();
    }
}

void Communication::listen(QString address, short port)
{
    tokenClientId = clientId;
    Node node;
    node.id = clientId;
    node.requested = 0;
    node.granted = 0;
    ricartTable.append(node);
    hasLock = false;
    tcpServer->listen(QHostAddress(address), port);
    qDebug() << "Listening";
}

void Communication::disconnectAndStopListening()
{
    qDebug() << "Stop listening & disconnect all nodes";
    tcpServer->close();
    ConnectionList::iterator i = clients.begin();
    while (i != clients.end())
    {
        QThread *thread = threads[i.key()];
        if(thread)
        {
            thread->quit();
            thread->wait();
        }
        threads.remove(i.key());
        qDebug() << "Close " << i.key() << " connection";
        ++i;
    }
    clients.clear();
    emit updateConnectionList(clients, tokenClientId, hasLock);
    emit disconnected();
}

void Communication::connectionError(int connectionId)
{
    QThread *thread = threads[connectionId];
    if(thread)
    {
        thread->quit();
        thread->wait();
    }
    threads.remove(connectionId);

    QJsonObject json;
    json.insert("type", "quit");
    json.insert("detectedBy", clientId);
    const QStringList &disconnectedClients = clients.values(connectionId);
    json.insert("clientList", QJsonArray::fromStringList(disconnectedClients));
    broadcastMessage(json, false);

    if(disconnectedClients.contains(tokenClientId))
    {
        clients.remove(connectionId);
        // recreate token
        tokenClientId = clientId;
        hasLock = false;
        emit lockChanged(hasLock, true);
        ricartTable.clear();
        foreach(QString clientId, clients.values())
        {
            Node node;
            node.id = clientId;
            node.requested = 0;
            node.granted = 0;
            ricartTable.append(node);
        }
        Node node;
        node.id = clientId;
        node.requested = 0;
        node.granted = 0;
        ricartTable.append(node);
    }
    else
    {
        foreach(const QString &clientId, clients.values(connectionId))
        {
            RicartTable::iterator i = ricartTable.begin();
            while(i != ricartTable.end())
            {
                if(clientId.compare(i->id) == 0)
                {
                    ricartTable.erase(i);
                    break;
                }
                else
                {
                    ++i;
                }
            }
        }
        clients.remove(connectionId);
    }

    emit updateConnectionList(clients, tokenClientId, hasLock);
    qDebug() << "connection " << connectionId << " closed";
}

void Communication::received(QJsonObject json, int id)
{
    forwardMessage(json, id, false);
    // parse message
    QString msgid = json["type"].toString();
    if(msgid.compare(msgid, "paint", Qt::CaseInsensitive) == 0)
    {
        QJsonValue color = json["color"];
        if(color != QJsonValue::Undefined)
        {
            QJsonValue value = json["pointList"];
            if(value != QJsonValue::Undefined)
            {
                QJsonArray pointArray = value.toArray();
                QQueue<QPoint> pointQueue;
                foreach (QJsonValue value, pointArray)
                {
                    QPoint point;
                    QJsonObject jsonPoint = value.toObject();
                    value = jsonPoint["x"];
                    if(value != QJsonValue::Undefined)
                    {
                        point.setX(value.toInt());
                        value = jsonPoint["y"];
                        if(value != QJsonValue::Undefined)
                        {
                            point.setY(value.toInt());
                            pointQueue.enqueue(point);
                        }
                    }
                }
                if(color.toInt() == 0)
                    emit lineReceived(pointQueue, Qt::black);
                else
                    emit lineReceived(pointQueue, Qt::white);
            }
        }
    }
    else if(msgid.compare(msgid, "clean", Qt::CaseInsensitive) == 0)
    {
        emit clearBoardReceived();
    }
    else if(msgid.compare(msgid, "joined", Qt::CaseInsensitive) == 0)
    {
        qDebug() << "joined";
        QJsonValue value = json["clientId"];
        if(value != QJsonValue::Undefined)
        {
            ConnectionList::iterator i = clients.find(id);
            if(i != clients.end() && !i.value().contains(value.toString()))
            {
                clients.insertMulti(id, value.toString());
                emit updateConnectionList(clients, tokenClientId, hasLock);
                qDebug() << "propagated join";
            }
            else
            {
                //qDebug() << "emit requestBoard";
                emit requestBoard(id, value.toString());
            }
            if(clientId.compare(tokenClientId) == 0)
            {
                Node node;
                node.id = value.toString();
                node.requested = 0;
                node.granted = 0;
                ricartTable.append(node);
            }
        }
    }
    else if(msgid.compare(msgid, "image", Qt::CaseInsensitive) == 0)
    {
        QByteArray byteArray;
        QJsonValue value = json["image"];
        if(value != QJsonValue::Undefined)
        {
            byteArray.append(value.toString());
            byteArray = QByteArray::fromBase64(byteArray);

            QJsonValue senderClientId = json["clientId"];
            if(value != QJsonValue::Undefined)
            {
                QJsonValue value = json["token"];
                if(value != QJsonValue::Undefined)
                {
                    QJsonObject jsonObject = value.toObject();
                    QJsonValue value = jsonObject["clientId"];
                    if(value != QJsonValue::Undefined)
                    {
                        tokenClientId = value.toString();
                        QJsonValue value = jsonObject["hasLock"];
                        if(value != QJsonValue::Undefined)
                        {
                            hasLock = value.toBool();
                            QJsonValue value = json["clientList"];
                            if(value != QJsonValue::Undefined)
                            {
                                QJsonArray clientArray = value.toArray();
                                foreach (QJsonValue value, clientArray)
                                {
                                    clients.insert(id, value.toString());
                                }
                                emit updateBoard(byteArray);
                                emit lockChanged(hasLock, true);
                                emit updateConnectionList(clients, tokenClientId, hasLock);
                                emit connected();
                            }
                        }
                    }
                }
            }
        }
    }
    else if(msgid.compare(msgid, "quit", Qt::CaseInsensitive) == 0)
    {
        QJsonValue detectedBy = json["detectedBy"];
        if(detectedBy != QJsonValue::Undefined)
        {
            QJsonValue value = json["clientList"];
            if(value != QJsonValue::Undefined)
            {
                QJsonArray clientArray = value.toArray();
                foreach (QJsonValue value, clientArray)
                {
                    clients.remove(id, value.toString());
                    if(tokenClientId.compare(value.toString()) == 0)
                    {
                        tokenClientId = detectedBy.toString();
                        hasLock = false;
                        emit lockChanged(hasLock, true);
                    }
                }
                emit updateConnectionList(clients, tokenClientId, hasLock);
            }
        }
    }
    else if(msgid.compare(msgid, "passToken", Qt::CaseInsensitive) == 0)
    {

        QJsonValue destClientId = json["destClientId"];
        if(destClientId != QJsonValue::Undefined)
        {
            if(clientId.compare(destClientId.toString()) == 0)
            {
                QJsonValue value = json["ricartTable"];
                if(value != QJsonValue::Undefined)
                {
                    qDebug() << "ricardTable: ";
                    ricartTable.clear();
                    ++logicalTime;
                    QJsonArray array = value.toArray();
                    foreach (QJsonValue value, array)
                    {
                        QJsonObject object = value.toObject();
                        QJsonValue id = object["clientId"];
                        if(id != QJsonValue::Undefined)
                        {
                            QJsonValue requested = object["lastRequestLogicalTime"];
                            if(requested != QJsonValue::Undefined)
                            {
                                QJsonValue granted = object["lastBlockadeLogicalTime"];
                                if(granted != QJsonValue::Undefined)
                                {
                                    qDebug() << id.toString();

                                    Node node;
                                    node.id = id.toString();
                                    node.requested = requested.toInt();
                                    if(clientId.compare(node.id) == 0)
                                    {
                                        node.granted = logicalTime;
                                    }
                                    else
                                    {
                                        node.granted = granted.toInt();
                                    }
                                    ricartTable.append(node);
                                }
                            }
                        }
                    }
                }
                tokenClientId = destClientId.toString();
                hasLock = true;
                emit lockChanged(true, false);
            }
            else
            {
                tokenClientId = destClientId.toString();
                hasLock = true;
                emit lockChanged(true, true);
            }
            emit updateConnectionList(clients, tokenClientId, hasLock);
        }
    }
    else if(msgid.compare(msgid, "request", Qt::CaseInsensitive) == 0)
    {
        QJsonValue value = json["clientId"];
        if(value != QJsonValue::Undefined)
        {
            QJsonValue logicalTime = json["logicalTime"];
            if(logicalTime != QJsonValue::Undefined)
            {
                if(clientId.compare(tokenClientId) == 0)
                {
                    for(int i=0; i<ricartTable.size(); ++i)
                    {
                        if(ricartTable[i].id.compare(value.toString()) == 0)
                        {
                            ricartTable[i].requested = logicalTime.toInt();
                            if(!hasLock)
                            {
                                passToken(value.toString());
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
    else if(msgid.compare(msgid, "unlock", Qt::CaseInsensitive) == 0)
    {
        QJsonValue value = json["clientId"];
        if(value != QJsonValue::Undefined)
        {
            hasLock = false;
            emit lockChanged(false, true);
            emit updateConnectionList(clients, tokenClientId, hasLock);
        }
    }
}

void Communication::forwardMessage(QJsonObject json, int sourceConnectionId, bool blocking)
{
    // pass to all except the node from which the message arrived
    IdList idList;
    ConnectionList::iterator i = clients.begin();
    while (i != clients.end())
    {
        if(i.key() != sourceConnectionId)
            idList.append(i.key());
        ++i;
    }
    if(blocking)
        emit sendBlocking(json, idList);
    else
        emit send(json, idList);
}

void Communication::singleMessage(QJsonObject json, int destinationConnectionId, bool blocking)
{
    IdList idList;
    idList.append(destinationConnectionId);
    if(blocking)
        emit sendBlocking(json, idList);
    else
        emit send(json, idList);
}

void Communication::broadcastMessage(QJsonObject json, bool blocking)
{
    if(blocking)
        emit sendBlocking(json, clients.keys());
    else
        emit send(json, clients.keys());
}

void Communication::lineDrawn(PointQueue pointQueue, QColor color)
{
    QJsonObject json;
    json.insert("type", "paint");
    json.insert("clientId", clientId);
    if(color == Qt::black)
        json.insert("color", 0);
    else
        json.insert("color", 255);
    QJsonArray pointArray;
    while(!pointQueue.isEmpty())
    {
        QPoint point = pointQueue.dequeue();
        QJsonObject jsonPoint;
        jsonPoint.insert("x", point.x());
        jsonPoint.insert("y", point.y());
        pointArray.append(jsonPoint);
    }
    json.insert("pointList", pointArray);

    // send to all nodes
    broadcastMessage(json, false);
}

void Communication::clearBoard()
{
    if(!hasLock || (clientId.compare(tokenClientId) == 0))
    {
        QJsonObject json;
        json.insert("type", "clean");
        json.insert("clientId", clientId);

        // send to all nodes
        broadcastMessage(json, false);
    }
}

void Communication::responseBoard(QByteArray pixels, int id, QString clientId)
{
    QJsonObject json;
    json.insert("type", "image");
    json.insert("clientId", this->clientId);
    QStringList clientList;
    ConnectionList::iterator i = clients.begin();
    while (i != clients.end())
    {
        clientList.append(i.value());
        ++i;
    }
    clientList.append(this->clientId);
    json.insert("clientList", QJsonArray::fromStringList(clientList));
    QJsonObject token;
    token.insert("clientId", tokenClientId);
    token.insert("hasLock", hasLock);
    json.insert("token", token);
    json.insert("image", QString(pixels.toBase64()));

    // send only to node which requested the board
    singleMessage(json, id, false);

    clients.insert(id, clientId);
    emit updateConnectionList(clients, tokenClientId, hasLock);
}

void Communication::tryLock()
{
    if(clientId.compare(tokenClientId) != 0)
    {
        ++logicalTime;
        QJsonObject json;
        json.insert("type", "request");
        json.insert("clientId", clientId);
        json.insert("logicalTime", logicalTime);
        broadcastMessage(json, false);
    }
    else
    {
        passToken(clientId);
    }
}

void Communication::passToken(QString dstClientId)
{
    if(clientId.compare(tokenClientId) == 0)
    {
        QJsonObject json;
        json.insert("type", "passToken");
        QJsonArray array;
        foreach(Node node, ricartTable)
        {
            QJsonObject object;
            object.insert("clientId", node.id);
            object.insert("lastRequestLogicalTime", node.requested);
            object.insert("lastBlockadeLogicalTime", node.granted);
            array.append(object);
        }
        json.insert("ricartTable", array);
        json.insert("destClientId", dstClientId);
        broadcastMessage(json, false);

        tokenClientId = dstClientId;
        hasLock = true;
        if(clientId.compare(dstClientId) == 0)
        {
            emit lockChanged(true, false);
        }
        else
        {
            emit lockChanged(true, true);
        }
        emit updateConnectionList(clients, tokenClientId, hasLock);
    }
}

void Communication::passTokenToNext()
{
    int startIdx = 0;
    foreach(Node node, ricartTable)
    {
        if(node.id.compare(clientId) == 0)
        {
            break;
        }
        ++startIdx;
    }
    QString dstClientId;
    bool found = false;
    for(int i = 0; i < ricartTable.size() - 1; ++i)
    {
        Node &node = ricartTable[(i + startIdx + 1) % ricartTable.size()];
        //qDebug() << "ricardTable[" << (i + startIdx + 1) % ricardTable.size() << "] = (" <<
        //            node.id << ", " << node.requested << ", " << node.granted << ")";
        if(node.requested > node.granted)
        {
            dstClientId = node.id;
            found = true;
            break;
        }
    }
    if(found)
    {
        passToken(dstClientId);
    }
}

void Communication::unlock()
{
    if(clientId.compare(tokenClientId) == 0 && hasLock)
    {
        QJsonObject json;
        json.insert("type", "unlock");
        json.insert("clientId", clientId);
        broadcastMessage(json, false);
        hasLock = false;
        emit lockChanged(false, false);
        emit updateConnectionList(clients, tokenClientId, hasLock);
        passTokenToNext();
    }
}
