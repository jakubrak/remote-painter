#include "connection.h"
#include <QByteArray>
#include <QStack>
#include <QJsonDocument>
#include "common.h"

Connection::Connection(int id, QTcpSocket *tcpSocket) :
    id(id),
    tcpSocket(tcpSocket)
{
    tcpSocket->setParent(this);
    tcpSocket->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
}

Connection::~Connection()
{
    if(tcpSocket)
    {
        if(tcpSocket->isOpen())
        {
            tcpSocket->close();
        }
        delete tcpSocket;
    }
}

void Connection::send(QJsonObject json, IdList idList)
{
    if(idList.contains(id))
    {
        QJsonDocument jsonDoc(json);
        QString strJson(jsonDoc.toJson(QJsonDocument::Compact));
        tcpSocket->write(strJson.toUtf8().append('\n')); // append a new line according to Bogdan's requirements
        QTime timestamp = QTime::currentTime();
        qDebug() << timestamp.hour() << timestamp.minute() << timestamp.second() << "SENT:" << strJson.toUtf8();
    }
}

void Connection::readyRead()
{
    //qDebug() << __FILE__ << ":" << __FUNCTION__ << "received " << tcpSocket->bytesAvailable() << " bytes";
    static QStack<char> stack;
    static QString data;
    do
    {
        char c;
        tcpSocket->read(&c, 1);
        switch (c) {
        case '{':
            stack.push(c);
            data.append(c);
            break;
        case '}':
            stack.pop();
            data.append(c);
            if(stack.isEmpty())
            {
                //got complete json document
                QJsonDocument jsonDoc = QJsonDocument::fromJson(data.toUtf8());
                //QTime timestamp = QTime::currentTime();
                //qDebug() << timestamp.hour() << timestamp.minute() << timestamp.second() <<"RECEIVED:" << data.toUtf8();
                LOG(data.toUtf8())
                emit received(jsonDoc.object(), id);
                data.clear();
            }
            break;
        default:
            if(!data.isEmpty())
            {
                data.append(c);
            }
            break;
        }
    }
    while(tcpSocket->bytesAvailable() > 0);
}

void Connection::socketError(const QAbstractSocket::SocketError &e)
{
    //qDebug() << e;
    emit error(id);
}
