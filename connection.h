#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QJsonObject>
#include <QStringList>

typedef QList<int> IdList;

class Connection : public QObject
{
    Q_OBJECT
public:
    explicit Connection(int id, QTcpSocket *tcpSocket);
    ~Connection();

signals:
    void received(QJsonObject json, int id);
    void error(int);

public slots:
    void send(QJsonObject json, IdList idList);

private slots:
    void readyRead();
    void socketError(const QAbstractSocket::SocketError &e);

private:
    QTcpSocket *tcpSocket;
    int id;

};

#endif // CONNECTION_H
