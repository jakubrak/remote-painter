#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <QTcpServer>
#include <QThread>
#include <QHash>
#include <QJsonObject>
#include <QJsonArray>
#include <QPoint>
#include <QQueue>
#include <QStringList>
#include <QTimer>
#include "connection.h"
#include "view.h"

typedef QQueue<QPoint> PointQueue;
typedef QList<int> IdList;
typedef QMultiMap<int, QString> ConnectionList;

typedef struct _node
{
    QString id;
    int granted;
    int requested;
} Node;

typedef QList<Node> RicartTable;

class Communication : public QObject
{
    Q_OBJECT

public:
    Communication(QString clientId);
    ~Communication();
    void forwardMessage(QJsonObject json, int sourceConnectionId, bool blocking);
    void singleMessage(QJsonObject json, int destinationConnectionId, bool blocking);
    void broadcastMessage(QJsonObject json, bool blocking);
    void closeConnection(int connectionId, bool notify);

public slots:

    // View
    void connectAndListen(QString remoteAddr, short remotePort,
                          QString localAddr, short localPort);
    void listen(QString address, short port);
    void disconnectAndStopListening();

    void responseBoard(QByteArray pixels, int id, QString clientId);
    void lineDrawn(PointQueue pointQueue, QColor color);
    void clearBoard();
    void passToken(QString dstClientId);
    void passTokenToNext();
    void tryLock();
    void unlock();

private slots:
    void connectionError(int connectionId);

signals:

    // Connection
    void send(QJsonObject json, IdList idList);
    void sendBlocking(QJsonObject json, IdList idList);

    // View
    void connected();
    void disconnected();

    void updateBoard(QByteArray pixels);
    void requestBoard(int connectionId, QString clientId);
    void lineReceived(PointQueue pointQueue, QColor color);
    void clearBoardReceived();
    void updateConnectionList(ConnectionList connectionList, QString tokenClientId, bool hasLock);
    void lockChanged(bool lock, bool remote);

private slots:

    // QTcpServer
    void openNewConnection();

    // Connection
    void received(QJsonObject json, int id);

private:

    QTcpServer *tcpServer;
    QHash<int, QThread*> threads;
    ConnectionList clients;
    QString clientId;
    int connectionIdGen;

    QString tokenClientId;
    bool hasLock;

    RicartTable ricartTable;
    int logicalTime;

    QTimer tokenTimer;
};

#endif // COMMUNICATION_H
