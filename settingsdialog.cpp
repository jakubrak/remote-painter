#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QDebug>
#include <QNetworkInterface>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    QRegExp ipRegex ("^" + ipRange + "\\." + ipRange + "\\." + ipRange + "\\." + ipRange + "$");
    QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);
    ui->lineEdit_remoteIp->setValidator(ipValidator);

    QIntValidator *portValidator = new QIntValidator(0, 65535, this);
    ui->lineEdit_remotePort->setValidator(portValidator);
    ui->lineEdit_localPort->setValidator(portValidator);

    ui->comboBox_lineColor->addItem("Black");
    ui->comboBox_lineColor->addItem("White");

    fillComboBox();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::showEvent(QShowEvent * event)
{
    //fillComboBox();
}

QString SettingsDialog::getRemoteIpAddress()
{
    return ui->lineEdit_remoteIp->text();
}

void SettingsDialog::setRemoteIpAddress(const QString &ipAddress)
{
    ui->lineEdit_remoteIp->setText(ipAddress);
}

int SettingsDialog::getRemotePort()
{
    return ui->lineEdit_remotePort->text().toInt();
}

void SettingsDialog::setRemotePort(int port)
{
    ui->lineEdit_remotePort->setText(QString::number(port));
}

QString SettingsDialog::getLocalNetInterface()
{
    QStringList list = ui->comboBox_localIp->currentText().split(" ");
    return list.last().remove('(').remove(')');
}

void SettingsDialog::setLocalNetInterface(const QString &netInterface)
{
    for(int i=0; i < ui->comboBox_localIp->count(); ++i)
    {
        if(ui->comboBox_localIp->itemText(i).contains(netInterface))
        {
            ui->comboBox_localIp->setCurrentIndex(i);
            break;
        }
    }
}

QString SettingsDialog::getLocalIpAddress()
{
    QStringList list = ui->comboBox_localIp->currentText().split(" ");
    return list.first();
}

int SettingsDialog::getLocalPort()
{
    return ui->lineEdit_localPort->text().toInt();
}

void SettingsDialog::setLocalPort(int port)
{
    ui->lineEdit_localPort->setText(QString::number(port));
}

void SettingsDialog::fillComboBox()
{
    ui->comboBox_localIp->clear();
    foreach(QNetworkInterface eth, QNetworkInterface::allInterfaces())
    {
        foreach (QNetworkAddressEntry entry, eth.addressEntries())
        {
            if(entry.ip().protocol() == QAbstractSocket::IPv4Protocol)
            {
                ui->comboBox_localIp->addItem(entry.ip().toString() + " (" + eth.name() + ")");
            }
        }
    }
}

int SettingsDialog::getLineColor()
{
    if(ui->comboBox_lineColor->currentText() == "Black")
        return 0;
    else
        return 255;
}

void SettingsDialog::setLineColor(int color)
{
    if(color == 0)
       ui->comboBox_lineColor->setCurrentIndex(0);
    else
       ui->comboBox_lineColor->setCurrentIndex(1);
}

