#include <QApplication>
#include <QProcess>
#include <QtGlobal>
#include <QTime>
#include "view.h"
#include "communication.h"

Q_DECLARE_METATYPE(QQueue<QPoint>)

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qRegisterMetaType< QQueue<QPoint> >();

    qsrand(QTime::currentTime().msec());
    QString clientId = "Jakub_" + QString::number((qrand() % (0xFFFFFFFF - 0x10000000)) + 0x10000000);

    View view(clientId);
    Communication communication(clientId);

    qRegisterMetaType<PointQueue>("PointQueue");
    qRegisterMetaType<IdList>("IdList");
    qRegisterMetaType<ConnectionList>("ConnectionList");

    QObject::connect(&view, SIGNAL(connectAndListen(QString,short,QString,short)),
                     &communication, SLOT(connectAndListen(QString,short,QString,short)));
    QObject::connect(&view, SIGNAL(listen(QString,short)), &communication, SLOT(listen(QString,short)));
    QObject::connect(&view, SIGNAL(disconnectAndStopListening()), &communication, SLOT(disconnectAndStopListening()));
    QObject::connect(&communication, SIGNAL(connected()), &view, SLOT(connected()));
    QObject::connect(&communication, SIGNAL(disconnected()), &view, SLOT(disconnected()));
    QObject::connect(&communication, SIGNAL(updateConnectionList(ConnectionList, QString, bool)), &view, SLOT(updateConnectionList(ConnectionList, QString, bool)));

    QObject::connect(&view, SIGNAL(unlock()), &communication, SLOT(unlock()));
    QObject::connect(&view, SIGNAL(tryLock()), &communication, SLOT(tryLock()));
    QObject::connect(&communication, SIGNAL(lockChanged(bool, bool)), &view, SLOT(lockChanged(bool, bool)));

    QObject::connect(&view, SIGNAL(responseBoard(QByteArray, int, QString)), &communication, SLOT(responseBoard(QByteArray, int, QString)));
    QObject::connect(&communication, SIGNAL(requestBoard(int, QString)), &view, SLOT(requestBoard(int, QString)));

    QObject::connect(&view, SIGNAL(clearBoard()), &communication, SLOT(clearBoard()));
    QObject::connect(&communication, SIGNAL(clearBoardReceived()), &view, SIGNAL(clearBoardReceived()));

    QObject::connect(&communication, SIGNAL(updateBoard(QByteArray)), &view, SLOT(updateBoard(QByteArray)));

    QObject::connect(&view, SIGNAL(lineDrawn(PointQueue, QColor)), &communication, SLOT(lineDrawn(PointQueue, QColor)));
    QObject::connect(&communication, SIGNAL(lineReceived(PointQueue, QColor)), &view, SIGNAL(lineReceived(PointQueue, QColor)));

    QThread thread;
    communication.moveToThread(&thread);
    thread.start();

    /*QString program = "./remote-painter";
    QStringList arguments;
    arguments << "child";

    if(argc == 1)
    {
        QProcess *myProcess = new QProcess();
        myProcess->startDetached(program, arguments);

    }*/

    view.show();
    int result = a.exec();
    thread.quit();
    thread.wait();
    return result;
}
