#-------------------------------------------------
#
# Project created by QtCreator 2015-11-01T18:29:10
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = remote-painter
TEMPLATE = app


SOURCES += main.cpp\
    outputboard.cpp \
    connection.cpp \
    view.cpp \
    atomicclock.cpp \
    communication.cpp \
    settingsdialog.cpp

HEADERS  += \
    outputboard.h \
    connection.h \
    view.h \
    atomicclock.h \
    communication.h \
    settingsdialog.h \
    common.h

FORMS    += mainwindow.ui \
    settingsdialog.ui
